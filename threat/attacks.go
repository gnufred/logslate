package threat

type AttackType byte

const (
	NIL AttackType = iota
	UNK            // Unknown or uncategorized.
	SQL            // SQL injection.
	CMD            // Command injection.
	DIR            // Directory traversal.
	XSS            // Cross site scripting.
	SCN            // Leak & portal scanning.
)

// Attack pattern structure.
// The Score scales from 1 to 10 meaning:
// * 1: it might be an attack but probably not.
// * 2-3: scale in between those two.
// * 5: we're pretty sure it's an attack.
// * 6+: not used.
type ptrn struct {
	Name  string
	Type  AttackType
	Score byte
}

// Attack patterns.
// Patterns that are commented with Score: 0, have to many false positive to be
// useful. I'm keeping them commented to avoid putting them back in the future.
var attackPatterns = []ptrn{

	// !!! Unknown attacks patterns !!! //
	// Patterns I've seen mixed in with other attacks.

	// {Name: "***", Type: UNK, Score: 1}, // False positive: w2.02, can be used in "hidden" credit card numbers
	// {Name: "{{", Type: UNK, Score: 0}, // False positive: w1.01
	// {Name: "}}", Type: UNK, Score: 0}, // False positive: w1.01
	{Name: "%40%40", Type: UNK, Score: 2},
	{Name: "'+", Type: SQL, Score: 2}, // In XSS, SQL, and legit URLs.
	{Name: "(((", Type: UNK, Score: 5},
	{Name: ")))", Type: UNK, Score: 5},
	{Name: ",,", Type: UNK, Score: 2},
	{Name: "\x22", Type: UNK, Score: 3}, // All over the place and never alone.

	// !!! SQL injection attacks !!! //
	// https://portswigger.net/web-security/sql-injection/cheat-sheet

	// Generic select.

	// {Name: "select%", Type: SQL, Score: 0}, "select%" is all over the place in GraphQL queries.
	{Name: "select+", Type: SQL, Score: 2}, // Some legit hits.
	{Name: "select ", Type: SQL, Score: 5},
	{Name: "select(", Type: SQL, Score: 5},

	// Substring.

	{Name: "substr(", Type: SQL, Score: 5},
	{Name: "substr%", Type: SQL, Score: 5},
	{Name: "substring(", Type: SQL, Score: 5},
	{Name: "substring%", Type: SQL, Score: 5},

	// Time delays.

	// {Name: "benchmark", Type: SQL, Score: 0},
	// {Name: "delay", Type: SQL, Score: 0},
	// {Name: "sleep", Type: SQL, Score: 0},
	{Name: "benchmark%", Type: SQL, Score: 4},
	{Name: "benchmark(", Type: SQL, Score: 5},
	{Name: "benchmark+", Type: SQL, Score: 5},
	{Name: "delay%", Type: SQL, Score: 4},
	{Name: "delay(", Type: SQL, Score: 5},
	{Name: "delay+", Type: SQL, Score: 2},
	{Name: "pg_sleep", Type: SQL, Score: 5},
	{Name: "receive_message", Type: CMD, Score: 5},
	{Name: "sleep%", Type: SQL, Score: 2},
	{Name: "sleep(", Type: SQL, Score: 5},
	{Name: "sleep+", Type: SQL, Score: 2},
	{Name: "waitfor%", Type: SQL, Score: 5},
	{Name: "waitfor(", Type: SQL, Score: 5},
	{Name: "waitfor+", Type: SQL, Score: 5},

	// DNS lookup.

	{Name: "exec master", Type: SQL, Score: 5},
	{Name: "extractvalue", Type: SQL, Score: 5},
	{Name: "get_host_address", Type: SQL, Score: 5},
	{Name: "load_file%28", Type: SQL, Score: 5},
	{Name: "load_file(", Type: SQL, Score: 5},

	// Uncategorized.

	{Name: "!%2b!", Type: SQL, Score: 5},
	{Name: "!((", Type: SQL, Score: 4},
	{Name: "!+!", Type: SQL, Score: 5},
	{Name: "!='", Type: SQL, Score: 5},
	{Name: "%21+%21", Type: SQL, Score: 5},
	{Name: "' ", Type: SQL, Score: 5},
	{Name: "'%", Type: SQL, Score: 2},
	{Name: "'%20and", Type: SQL, Score: 5},
	{Name: "'%20or", Type: SQL, Score: 5},
	{Name: "'%22", Type: SQL, Score: 5},
	{Name: "'%7d", Type: SQL, Score: 4},
	{Name: "'%bf", Type: SQL, Score: 5},
	{Name: "'%f0", Type: SQL, Score: 5},
	{Name: "''", Type: SQL, Score: 1}, // False positive: 9
	{Name: "')", Type: SQL, Score: 1}, // False positive: 9
	{Name: "'1=0", Type: SQL, Score: 5},
	{Name: "'1=1", Type: SQL, Score: 5},
	{Name: "'=", Type: SQL, Score: 5},
	{Name: "'and", Type: SQL, Score: 5},
	{Name: "'or", Type: SQL, Score: 5},
	{Name: "'xor", Type: SQL, Score: 5},
	{Name: "'|", Type: SQL, Score: 5},
	{Name: ",null", Type: SQL, Score: 5},
	{Name: "--+", Type: SQL, Score: 5},
	{Name: "and%20'", Type: SQL, Score: 5},
	{Name: "null:null", Type: SQL, Score: 5},
	{Name: "or%20'", Type: SQL, Score: 5},
	{Name: "qsqli_", Type: SQL, Score: 5},
	{Name: "xor\\", Type: SQL, Score: 5},

	// !!! Directory traversal attacks !!! //
	// https://portswigger.net/web-security/file-path-traversal

	// {Name: "%252f", Type: Dir, Score: 0},
	// {Name: "\x5c", Type: Dir, Score: 0},
	{Name: "%252e", Type: DIR, Score: 2}, // Some legit hits.
	{Name: "%2557EB-INF", Type: DIR, Score: 5},
	{Name: "%2e%2e/%2e%2e", Type: DIR, Score: 5},
	{Name: "%2e%2e//%2e%2e", Type: DIR, Score: 5},
	{Name: "%2e%2f", Type: DIR, Score: 4},
	{Name: "%2e%5c", Type: DIR, Score: 4},
	{Name: ".%10c", Type: DIR, Score: 4},
	{Name: ".%2550", Type: DIR, Score: 4},
	{Name: ".%25c0", Type: DIR, Score: 4},
	{Name: ".%2f", Type: DIR, Score: 4},
	{Name: ".%5c", Type: DIR, Score: 4},
	{Name: ".%c0%af", Type: DIR, Score: 4},
	{Name: ".%c1%9c", Type: DIR, Score: 4},
	{Name: ".%ef%bc%8f", Type: DIR, Score: 4},
	{Name: "./", Type: DIR, Score: 2},
	{Name: ".0x5c", Type: DIR, Score: 4},
	{Name: ".ini", Type: DIR, Score: 2},
	{Name: "/passwd", Type: DIR, Score: 3},
	{Name: "WEB-INF", Type: DIR, Score: 5},
	{Name: "c:%", Type: DIR, Score: 5},
	{Name: "etc%252fpass", Type: DIR, Score: 5},
	{Name: "etc%2f", Type: DIR, Score: 5},
	{Name: "etc%5c", Type: DIR, Score: 5},
	{Name: "etc/", Type: DIR, Score: 2},
	{Name: "etc/passwd", Type: DIR, Score: 5},
	{Name: "file:%2f%2f", Type: DIR, Score: 5},
	{Name: "file://", Type: DIR, Score: 4},
	{Name: "unix:A", Type: DIR, Score: 5},
	{Name: "webadmin.ini", Type: DIR, Score: 5},
	{Name: "win.ini", Type: DIR, Score: 5},

	// !!! OS command injection attacks !!! //
	// https://portswigger.net/web-security/os-command-injection

	{Name: "%00{.", Type: CMD, Score: 5}, // CVE-2014-6287 https://packetstormsecurity.com/files/128243/HttpFileServer-2.3.x-Remote-Command-Execution.html
	{Name: "chr%", Type: CMD, Score: 1},  // Can match German "spiegelschr".
	{Name: "chr(", Type: CMD, Score: 5},
	{Name: "concat%", Type: CMD, Score: 5},
	{Name: "concat(", Type: CMD, Score: 5}, // In SQL injec but mostly in OS inject.
	{Name: "echo", Type: CMD, Score: 1},    // In legit URLs and attacks.
	{Name: "gethostbyname", Type: CMD, Score: 5},
	{Name: "hex(", Type: CMD, Score: 5},
	{Name: "ipconfig", Type: CMD, Score: 5},
	{Name: "netstat", Type: CMD, Score: 5},
	{Name: "nslookup", Type: CMD, Score: 5},
	{Name: "ps -", Type: CMD, Score: 5},
	{Name: "ps%20%2D", Type: CMD, Score: 5},
	{Name: "socket", Type: CMD, Score: 1}, // Legit word. In attacks, with other triggers.
	{Name: "tasklist", Type: CMD, Score: 2},
	{Name: "uname", Type: CMD, Score: 1}, // "uname" matches lots of words.
	{Name: "whoami", Type: CMD, Score: 5},

	// !!! Cross site scripting attacks !!! //
	// https://portswigger.net/web-security/cross-site-scripting/cheat-sheet
	// Wasn't nearly as complete as what I've seen. Added my observations.

	// JS and browser function injections.

	// {Name: "log", Type: XSS, Score: 0}, // Hits "blog", "catalog", and such.
	{Name: "alert%", Type: XSS, Score: 2}, // In legit URLs.
	{Name: "alert(", Type: XSS, Score: 5},
	{Name: "assert%", Type: XSS, Score: 5},
	{Name: "assert(", Type: XSS, Score: 5},
	{Name: "function(", Type: XSS, Score: 4},
	{Name: "genxor.close()", Type: XSS, Score: 5},
	{Name: "interact.sh", Type: XSS, Score: 5}, // https://github.com/projectdiscovery/interactsh
	{Name: "log%28", Type: XSS, Score: 1},
	{Name: "log(", Type: XSS, Score: 5},
	{Name: "md5%", Type: XSS, Score: 2},
	{Name: "md5(", Type: XSS, Score: 5},
	{Name: "print%", Type: XSS, Score: 2},
	{Name: "print'", Type: XSS, Score: 5},
	{Name: "print(", Type: XSS, Score: 4}, // Has some legit uses.
	{Name: "random(", Type: XSS, Score: 4},
	{Name: "replace(", Type: XSS, Score: 5},
	{Name: "replace;", Type: XSS, Score: 5},
	{Name: "replace\\", Type: XSS, Score: 5},
	{Name: "sendBeacon%", Type: XSS, Score: 5},
	{Name: "sendBeacon(", Type: XSS, Score: 5},
	{Name: "write%", Type: XSS, Score: 2}, // Has legit uses.
	{Name: "write(", Type: XSS, Score: 4},

	// HTML, JS tag injection.

	{Name: "<!--", Type: XSS, Score: 5},
	{Name: "=)(", Type: XSS, Score: 5},
	{Name: "%3D%29%28", Type: XSS, Score: 5},
	{Name: "esi:include", Type: XSS, Score: 5},
	{Name: "iframe%", Type: XSS, Score: 1},
	{Name: "iframe_", Type: XSS, Score: 2},
	{Name: "iframe_src", Type: XSS, Score: 5},
	{Name: "injected_by", Type: XSS, Score: 5},
	{Name: "script=", Type: XSS, Score: 4}, // False positive: w2.03
	{Name: "script>", Type: XSS, Score: 5},
	{Name: "script&gt", Type: XSS, Score: 5},
	{Name: "script%3e", Type: XSS, Score: 5},
	{Name: "script%2f%26", Type: XSS, Score: 5},
	{Name: "%253a%252f%252flocalhost%252f", Type: XSS, Score: 5}, // Double URL encoded "://localhost/".
	{Name: "document.domain", Type: XSS, Score: 5},
	{Name: "void%280%29", Type: XSS, Score: 5}, // From URL encoded "javascript:void(0)".

	// Config scans.

	// {Name: ".env", Type: Scn, Score: 5}, // Too many IPs scanning this.
	// {Name: "auth.js", Type: Scn, Score: 5}, // Not sure if that's usually legit or not.
	// {Name: "config.js", Type: Scn, Score: 5}, // To many legit hits.
	// {Name: "install.php", Type: Scn, Score: 5}, // To many IPs scanning this.
	// {Name: "sftp-config.json", Type: Scn, Score: 5}, // Sublime scans from dev workstations. Malicious actors also.
	// {Name: "sftp.json", Type: Scn, Score: 5}, // VSCode scans from dev workstations. Malicous actors also.
	{Name: "/keys.js", Type: SCN, Score: 5},
	{Name: "Cargo.lock", Type: SCN, Score: 5},
	{Name: "Cargo.toml", Type: SCN, Score: 5},
	{Name: "DLPCenter/loginform.sms", Type: SCN, Score: 5},
	{Name: "Dockerrun.aws.json", Type: SCN, Score: 5},
	{Name: "Gemfile", Type: SCN, Score: 5},
	{Name: "Guardfile", Type: SCN, Score: 5},
	{Name: "Login.action", Type: SCN, Score: 5},
	{Name: "Pipfile", Type: SCN, Score: 5},
	{Name: "Pipfile.lock", Type: SCN, Score: 5},
	{Name: "Procfile", Type: SCN, Score: 5},
	{Name: "apps.tidy.infinity.json", Type: SCN, Score: 5},
	{Name: "appsettings.", Type: SCN, Score: 5},
	{Name: "aws.sh", Type: SCN, Score: 5},
	{Name: "awstats.conf", Type: SCN, Score: 5},
	{Name: "azure-pipelines.yml", Type: SCN, Score: 5},
	{Name: "azuredeploy.json", Type: SCN, Score: 5},
	{Name: "bitbucket-pipelines.yml", Type: SCN, Score: 5},
	{Name: "config.html", Type: SCN, Score: 5},
	{Name: "config.inc.php", Type: SCN, Score: 5},
	{Name: "credentials.db", Type: SCN, Score: 5},
	{Name: "dataOrigin.ashx", Type: SCN, Score: 5},
	{Name: "databases.yml", Type: SCN, Score: 5},
	{Name: "debug.log", Type: SCN, Score: 5},
	{Name: "default.json", Type: SCN, Score: 5},
	{Name: "deploy.sh", Type: SCN, Score: 5},
	{Name: "desktop.ini", Type: SCN, Score: 5},
	{Name: "development.log", Type: SCN, Score: 5},
	{Name: "development.log", Type: SCN, Score: 5},
	{Name: "env.js", Type: SCN, Score: 5},
	{Name: "env.sh", Type: SCN, Score: 5},
	{Name: "error.log", Type: SCN, Score: 5},
	{Name: "errors.log", Type: SCN, Score: 5},
	{Name: "firewall.log", Type: SCN, Score: 5},
	{Name: "go.mod", Type: SCN, Score: 5},
	{Name: "htaccess.txt", Type: SCN, Score: 5},
	{Name: "init.sh", Type: SCN, Score: 5},
	{Name: "karma.conf.js", Type: SCN, Score: 5},
	{Name: "karma.js", Type: SCN, Score: 5},
	{Name: "keycloak.json", Type: SCN, Score: 5},
	{Name: "kustomization.yml", Type: SCN, Score: 5},
	{Name: "login.action", Type: SCN, Score: 5},
	{Name: "mobile.log", Type: SCN, Score: 5},
	{Name: "parameters.yml", Type: SCN, Score: 5},
	{Name: "privatekey.key", Type: SCN, Score: 5},
	{Name: "production.log", Type: SCN, Score: 5},
	{Name: "run.sh", Type: SCN, Score: 5},
	{Name: "secret_token.rb", Type: SCN, Score: 5},
	{Name: "secrets.yml", Type: SCN, Score: 5},
	{Name: "security.yaml", Type: SCN, Score: 5},
	{Name: "sendgrid.ev", Type: SCN, Score: 5},
	{Name: "settings.py", Type: SCN, Score: 5},
	{Name: "settings.yml", Type: SCN, Score: 5},
	{Name: "user.xml", Type: SCN, Score: 5},
	{Name: "vpn.log", Type: SCN, Score: 5},

	// Admin panel scans.
	// As of right now, I'm conservative.
	// The idea is to flag paths that aren't used by legit users.
	// I might change that later, but given the simplist nature of the detectiong logic,
	// I'm keeping it simple.
	// In any case, attackers will almost always scan weird paths that nobody uses.

	// {Name: "/cgi-bin", Type: Scn, Score: 5}, // To many legit hits.
	// {Name: "/wp-login.php", Type: Scn, Score: 5}, // Too many IPs scanning this.
	{Name: "/setup.php", Type: SCN, Score: 5},
	{Name: "CgiStart", Type: SCN, Score: 5},
	{Name: "FNDWRR.exe", Type: SCN, Score: 5},
	{Name: "Telerik.Web.UI.DialogHandler", Type: SCN, Score: 5},
	{Name: "_adminer/index.php", Type: SCN, Score: 5},
	{Name: "adminer.php", Type: SCN, Score: 5},
	{Name: "adminer/adminer.php", Type: SCN, Score: 5},
	{Name: "b9dmx.php", Type: SCN, Score: 5},
	{Name: "backup/auto.php", Type: SCN, Score: 5},
	{Name: "backup2.cgi", Type: SCN, Score: 5},
	{Name: "cache/backup", Type: SCN, Score: 5},
	{Name: "carbon/admin/login.jsp", Type: SCN, Score: 5},
	{Name: "cfg/login", Type: SCN, Score: 5},
	{Name: "passwordrecovered.cgi", Type: SCN, Score: 5},
	{Name: "phpmyadmin", Type: SCN, Score: 5},
	{Name: "qmailadmin.cgi", Type: SCN, Score: 5},
}

// DataDumpPatterns create the pattern list to detect data dumps.
// Added to attackPatterns in ahocorasick.init().
func DataDumpPatterns() []ptrn {
	file := []string{
		"api",
		"backup",
		"bak",
		"bin",
		"db",
		"db_backup",
		"dbdump",
		"dump",
		"html",
		"html",
		"old",
		"public",
		"release",
		"root",
		"site",
		"test",
		"www",
	}
	ext := []string{
		"sql",
		"7z",
		"bz2",
		"gz",
		"lz",
		"rar",
		"tar",
		"zip",
	}

	dump := make([]ptrn, 0, len(file)*len(ext))
	for _, f := range file {
		for _, e := range ext {
			dump = append(dump, ptrn{
				Name:  f + "." + e,
				Type:  SCN,
				Score: 5,
			})
		}
	}

	return dump
}

// String translate an attack type byte to a human readable string
func String(atk AttackType) string {
	switch atk {
	case UNK:
		return "???"
	case SQL:
		return "sql"
	case CMD:
		return "cmd"
	case DIR:
		return "dir"
	case XSS:
		return "xss"
	case SCN:
		return "scn"
	default:
		return ""
	}
}
