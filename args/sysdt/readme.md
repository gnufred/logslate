sysdt.go and sysdt_test.go are based on the excellent work of https://github.com/trstringer/go-systemd-time. That being said, I ended up changing to much so I just rewrote most of it.

That being said, I still owe him a lot of hours for this amazing code!

Thanks a lot!
